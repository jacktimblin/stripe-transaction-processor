FROM golang:1.8-alpine

ADD . /go/src/transaction-processor

RUN mkdir /data

ADD config.json /data

#Do a git clone of the boltzap-core.
RUN mkdir /root/.ssh/

# Copy over private key, and set permissions
ADD docker /root/.ssh/id_rsa

RUN chmod 600 /root/.ssh/id_rsa

# Create known_hosts
RUN touch /root/.ssh/known_hosts

RUN set -xe && \
    apk add --no-cache --virtual .build-deps git openssh && \
    ssh-keyscan gitlab.com >> /root/.ssh/known_hosts && cd /go/src && \
    git clone git@gitlab.com:jacktimblin/core-api.git core && \
    cd /go/src/transaction-processor && go get && go build && go install && \
    apk del .build-deps && rm -rf /root/.ssh

ENTRYPOINT ["/go/bin/transaction-processor", "--config", "/data/config.json"]