package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"net/http"
	"core/service"
	"core/model"
	"flag"
	"os"
	"github.com/stripe/stripe-go/client"
	"core/config"
	"core/event"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"reflect"
	"github.com/pkg/errors"
	"github.com/google/uuid"
	"encoding/json"
	"bytes"
	"strings"
	"github.com/stripe/stripe-go"
	"time"
	"fmt"
	"net/url"
	"unicode"
	"core"
)

const (
	//ConfigWebHookQueue The key in config to get the webhook NSQd config.
	ConfigWebHookQueue = "webhook_queue"
	//WebHookTopic The topic on NSQ which describes webhook events.
	WebHookTopic = "webhook"
	//EventIDHeader The header in which the re-scheduler places the request id.
	EventIDHeader = "X-Event-Request-ID"
	//DefaultHealthCheckPath The default health check path.
	DefaultHealthCheckPath = "/healthz"
)

//TODO - TransactionProcessor needs to be put in its own package in core so it can be used for different payment Gateways.

//StripeEvent An encapsulation of a stripe event, only holds the
//id as we verify the event with stripe.
type StripeEvent struct {
	ID string `json:"id" form:"id" query:"id"`
	Account string `json:"account" form:"account" query:"account"`
	Type string `json:"type" form:"type" query:"type"`
}

//TransactionEventHandler A wrapper for a echo.HandlerFunc
type TransactionEventHandler func(t *TransactionProcessor, evt interface{}) error

//HealthCheckEventHandler A wrapper for a defined health check.
type HealthCheckEventHandler echo.HandlerFunc

//DefaultHealthCheckHandler The default health check handler function.
var DefaultHealthCheckHandler = func (c echo.Context) error {
	logger, _ := zap.NewDevelopment()
	logger.Info("/healthz check triggered, responding...")
	return c.JSON(http.StatusOK, DefaultSuccessResponse)
}

//DefaultSuccessResponse The default success response.
var DefaultSuccessResponse echo.Map = echo.Map{"success": true}

//DefaultErrorResponse The default error response
var DefaultErrorResponse echo.Map = echo.Map{"success": false}

//TransactionProcessor The transaction processor, handles events from payment gateway webhooks.
type TransactionProcessor struct {
	//AuthService the auth service to authenticate and retrieve users.
	AuthService *service.AuthService
	//Config The config object.
	Config config.Config
	//webhookQueue A seperate NSQ service to send to the NSQd instance which handles webhook events.
	webHookQueue *service.QueueService
	//TransactionService the service which handles manipulating and retrieving transactions.
	TransactionService *service.TransactionService
	//Gateway The gateway this processor is for.
	Gateway int
	//Endpoint the endpoint to publish requests to, used for re-scheduling and building the route in echo.
	Endpoint core.URL
	//HealthCheck the endpoint handler for a health-check.
	HealthCheck struct {
		//Path the route path.
		Path string
		//Handler the health-check handler.
		Handler HealthCheckEventHandler
	}
	//The handler to handle events, this is essentially a wrapper which gives the handler access to the processor.
	Handler TransactionEventHandler
	//Logger the internal logger to use.
	Logger *zap.Logger
	//Event the type of structure to un-marshal request data into.
	Event interface{}
	//server The internal server implementation.
	server *echo.Echo
}

//rescheduleEvent Reschedules an event to be processed.
func (t *TransactionProcessor) rescheduleEvent(event event.Webhook) error {
	if !t.webHookQueue.Ready {
		return nil
	}

	return t.webHookQueue.PublishMessage(service.QueueMessage{
		Topic: WebHookTopic,
		Message: event,
	})
}

//Notify notifies NSQ when a new transaction has been created
func (t *TransactionProcessor) Notify(evt event.Webhook) error {
	t.Info("notifying queue of new transactions")
	if !t.AuthService.Queue.Ready {
		return nil
	}

	return t.AuthService.Queue.PublishMessage(service.QueueMessage{
		Topic: event.TransactionTopic,
		Message: evt,
	})
}

//Get helper function, calls Config.Get
func (t *TransactionProcessor) Get(key string, data interface{}) error {
	return t.Config.Get(key, data)
}

//Info wrapper to the info function on logger.
func (t *TransactionProcessor) Info(msg string, fields ...zapcore.Field) {
	t.Logger.Info(msg, fields...)
}

//Warn wrapper to the warn function on logger.
func (t *TransactionProcessor) Warn(msg string, fields ...zapcore.Field) {
	t.Logger.Warn(msg, fields...)
}

//Error wrapper to the error function on logger.
func (t *TransactionProcessor) Error(msg string, fields ...zapcore.Field) {
	t.Logger.Error(msg, fields...)
}

//Process Processes an event, if the TransactionEventHandler returns err the event is rescheduled into NSQ.
func (t *TransactionProcessor) process() echo.HandlerFunc {
	//Generate the echo handler.
	return func(c echo.Context) error {
		//Ensure a post request was performed.
		if c.Request().Method != http.MethodPost {
			t.Info("request method has to be POST")
			return c.JSON(http.StatusBadRequest, DefaultErrorResponse)
		}

		typ := reflect.TypeOf(t.Event)

		if typ == nil {
			t.Info("Event type not defined.")
			return c.JSON(http.StatusBadRequest, DefaultErrorResponse)
		}

		val := reflect.New(typ)

		if val.IsNil() {
			t.Info("Event type not defined.")
			return c.JSON(http.StatusBadRequest, DefaultErrorResponse)
		}

		evt := val.Interface()

		//Parse the request details based on the content type header.
		ctype := c.Request().Header.Get(echo.HeaderContentType)

		switch {
		case strings.HasPrefix(ctype, echo.MIMEApplicationJSON):
			buf := new(bytes.Buffer)
			buf.ReadFrom(c.Request().Body)
			defer c.Request().Body.Close()
			body := buf.Bytes()
			if err := json.Unmarshal(body, evt); err != nil {
				t.Info("Could not bind event object")
				return c.JSON(http.StatusBadRequest, DefaultErrorResponse)
			}
			//We can handle other content-types in the future, but Stripe is JSON.
			//case strings.HasPrefix(ctype, echo.MIMEApplicationXML):
			break
		default:
			t.Info("Invalid Content-Type header")
			return c.JSON(http.StatusBadRequest, DefaultErrorResponse)
		}

		//attempt to handle event.
		t.Info("Attempting to handle request from: " + c.Request().Host)

		eventID := c.Request().Header.Get(EventIDHeader)

		//recover from panics by rescheduling the event.
		defer func(t *TransactionProcessor, eventID string, evt interface{}) {
			if r := recover(); r != nil {
				var err error
				switch r := r.(type) {
				case error:
					err = r
				default:
					err = fmt.Errorf("%v", r)
				}
				t.handleError(err, eventID, evt)
			}
		}(t, eventID, evt)

		if err := t.Handler(t, evt); err != nil {
			//Get the event Id from the Request header.
			t.handleError(err, eventID, evt)
		}

		t.Info("Event consumed.")
		return c.JSON(http.StatusOK, DefaultSuccessResponse)
	}
}

//handleError handles error by re-scheduling an event if the event id is not defined.
func (t *TransactionProcessor) handleError(err error, eventID string, evt interface{}) {
	t.Info("Failed to handle event with error: " + err.Error() + ", rescheduling...")
	//Set the initial event id if this is the first time we are rescheduling.
	if eventID == "" {
		t.Info("Set request id to: " + eventID)
		t.rescheduleEvent(event.Webhook{
			Endpoint: t.Endpoint,
			Gateway: t.Gateway,
			Event: evt,
			Method: core.Method(http.MethodPost),
		})
		t.Info("Request rescheduled.")
	} else {
		t.Info("Event has already been rescheduled to NSQ with id: " + eventID + ", skipping...")
	}
}

//Listen listens on the configured or default port.
func (t *TransactionProcessor) Listen() {
	//Ensure the handler is defined.
	if t.Handler == nil {
		panic(errors.New("handler is not defined"))
	}
	//Initialise the web-hook handler on the server.
	t.server.POST(t.Endpoint.Path, t.process())
	//Initialise the health check.
	if t.HealthCheck.Path != "" {
		if t.HealthCheck.Handler == nil {
			t.HealthCheck.Handler = DefaultHealthCheckHandler
		}
		t.server.GET(t.HealthCheck.Path, echo.HandlerFunc(t.HealthCheck.Handler))
	}

	port := t.Endpoint.Port()

	//Set the default port if its not set.
	if port == "" {
		port = config.DefaultPort
	}
	//Start listening on the defined port.
	t.server.Logger.Fatal(t.server.Start(":" + port))
}

//NewTransactionProcessor Initialises a transaction processor with default values.
func NewTransactionProcessor(cnf config.Config) TransactionProcessor {
	logger, err := zap.NewDevelopment()

	if err != nil {
		panic(err)
	}

	db := service.NewDatabaseConnection()
	err = cnf.Get(service.ConfigConnection, &db)

	if err != nil {
		panic(err)
	}

	if db.Database == "" {
		flag.PrintDefaults()
		os.Exit(1)
	}

	queue := service.NewQueueConfig()
	err = cnf.Get(service.ConfigQueue, &queue)

	if err != nil {
		panic(err)
	}

	authConfig := service.NewAuthConfig()
	authConfig.DatabaseConnection = db
	authConfig.Queue = queue

	auth, err := service.NewAuthService(authConfig)

	if err != nil {
		panic(err)
	}

	server := echo.New()
	server.Use(middleware.Secure())

	queueCnf := service.NewQueueConfig()
	cnf.Get(ConfigWebHookQueue, &queueCnf)

	return TransactionProcessor{
		Logger: logger,
		AuthService: auth,
		TransactionService: service.NewTransactionService(auth.GetConnection(), auth.Queue),
		Config: cnf,
		server: server,
		webHookQueue: service.NewQueueService(queueCnf),
		HealthCheck: struct {
			Path    string
			Handler HealthCheckEventHandler
		}{Path: DefaultHealthCheckPath, Handler: DefaultHealthCheckHandler},
	}
}

func main() {
	cnf, err := config.ParseConfig()

	if err != nil {
		panic(err)
	}

	strp := new(config.StripeConfig)
	err = cnf.Get(config.ConfigStripe, strp)
	if err != nil {
		panic(err)
	}

	transactionProcessor := NewTransactionProcessor(cnf)
	//set the gateway this processor is for.
	transactionProcessor.Gateway = model.GatewayStripe
	//set the endpoint.
	transactionProcessor.Endpoint = strp.Handler
	//We can set what type of struct can be un-marshalled from the request query, if this
	//is nil an error will be thrown as this also gets sent to the reschedule event.
	transactionProcessor.Event = StripeEvent{}
	transactionProcessor.Handler = func (t *TransactionProcessor, data interface{}) error {
		var ok bool
		var ev *StripeEvent

		if ev, ok = data.(*StripeEvent); !ok {
			return errors.New("An error occurred parsing the event details")
		}

		t.Info("Received event with id: " + ev.ID)

		api := &client.API{}
		api.Init(strp.ApiKey, nil)

		t.Info("Attempting to find user the event belongs to")

		user, found, err := t.AuthService.FindUserByStripeID(ev.Account)

		if err != nil {
			return errors.New("an error occurred finding user relating to event: " + ev.ID)
		}

		if !found {
			//either the user is deleted, or they have de-authorised stripe from their account
			t.Info("could not find user, notifying stripe...")
			clnt := &http.Client{}
			req, _ := http.NewRequest("POST",
				"https://connect.stripe.com/oauth/deauthorize",
				strings.NewReader(url.Values{
					"client_id": {strp.ClientID},
					"stripe_user_id": {ev.Account},
				}.Encode()),
			)
			req.Header.Add("Authorization", "Bearer " + strp.ApiKey)
			clnt.Do(req)
			return nil
		}

		t.Info("Found user with uuid: " + user.UUID)

		//check if the type is an account authorisation
		//we cannot verify this event type because we no longer have access to make api requests
		//on the this users behalf if this is the event type.
		if ev.Type == model.TypeAccountDeauthorized {
			t.Info("Account Deauthorized event, removing stripe credentials from user: " + user.UUID)
			//The user de-authorized us from accessing their stripe account.
			t.AuthService.RemoveStripe(user)

			return nil
		}

		t.Info("Verifing event with Stripe")
		evt, err := api.Events.Get(ev.ID, &stripe.Params{StripeAccount: ev.Account})

		if err != nil {
			return err
		}

		//ensure that the user id is set.
		evt.Account = ev.Account

		if evt.Account == "" {
			return errors.New("could not verify event")
		}

		t.Info("Event verified")

		//objectID will be the charge.ID on a refund event not the refund.ID, we can get the parent transaction
		//if it exists.
		objectID := evt.GetObjValue("id")
		t.Info("Checking for existing transaction with the id: " + objectID)
		transaction, found, err := t.TransactionService.FindTransaction(model.GatewayStripe, objectID)

		if err != nil {
			return err
		}

		if !found && evt.Type == model.TypePayoutPaid {
			if user.Stripe.ProcessTransfers {
				//we have a payout paid event.
				payout := &stripe.Payout{}
				err = json.Unmarshal(evt.Data.Raw, payout)
				t.Info("Handling payout.paid event with event id: " + payout.ID)

				trans := &model.Transaction{
					GatewayID:          payout.ID,
					GatewayEventID:     evt.ID,
					TransactionCreated: time.Unix(payout.Created, 0),
					Captured:           true,
					Amount:             payout.Amount,
					Currency:           string(payout.Currency),
					Source: struct {
						Type string
						ID   string
					}{Type: string(payout.SourceType)},
					Status:          string(payout.Status),
					Description:     "Payout from Stripe, id: " + payout.ID,
					GatewayAccountID: evt.Account,
					UserID:          user.UUID,
					Gateway:         model.GatewayStripe,
					Type:            model.TypePayout,
					UUID:            uuid.New().String(),
					Version:         model.TransactionSchemaVersion,
				}

				switch payout.SourceType {
				case "bank_account":
					trans.Source.ID = payout.Bank.ID
					break
				case "card":
					trans.Source.ID = payout.Card.ID
					break
				}

				err = t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(trans)
				if err != nil {
					t.Error(err.Error())
				} else {
					t.Info("Transaction " + trans.UUID + " created")
				}
			} else {
				t.Info("Transfer processing disabled for user: " + user.UUID)
			}

			t.Info("Event consumed, notifying Stripe of success")

			//push this event to the transactions queue.
			t.Notify(event.Webhook{
				Type: event.TransactionCreated,
				Gateway: t.Gateway,
				Event: event.Transaction{
					UUID: user.UUID,
					EventID: evt.ID,
				},
			})

			return nil
		} else if evt.Type == model.TypePayoutPaid {
			t.Info("Payout with id: " + transaction.UUID + " exists, skipping...")
			return nil
		}

		charge := &stripe.Charge{}
		err = json.Unmarshal(evt.Data.Raw, charge)
		t.Info("Handling " + evt.Type + " event, with charge id: " + charge.ID)
		var balanceTransaction *stripe.Transaction

		if !charge.Captured {
			//we don't care about un-captured charges.
			//inform stripe we have received and handled this event.
			t.Info("charge is not captured, skipping...")
			return nil
		}

		//determine the balance_transaction id to use (this depends on whether
		//its a new charge or a refund event.
		var balanceTransactionId string
		balanceTransactionId = charge.Tx.ID

		//check that we have a valid balance_transaction id.
		if balanceTransactionId != "" {
			//retrieve details of the fees applied to this charge.
			//only if the user have the process fees option on.
			//no point making the extra request to stripe if we don't need to.
			if user.Stripe.ProcessFees {
				transaction, err := api.Balance.GetTx(
					balanceTransactionId,
					&stripe.TxParams{Params: stripe.Params{StripeAccount: evt.Account}},
				)
				if err != nil {
					return err
				}
				balanceTransaction = transaction
			}
		}

		if err != nil {
			return err
		}

		fee := &model.Transaction{}
		if !found && (evt.Type == model.TypeChargeCaptured || evt.Type == model.TypeChargeSucceeded) {
			transaction = processCharge(t, user, charge, evt, balanceTransaction)
		} else if evt.Type == model.TypeChargeRefunded {
			if !found {
				transaction = processCharge(t, user, charge, evt, balanceTransaction)
			}
			//stripe by convenience provides the 10 most recent refunds for a charge.
			if charge.Refunds.Count > 0 {
				for _, refund := range charge.Refunds.Values {
					processRefund(t, evt, refund, user, transaction, api, !found)
				}
				if charge.Refunds.More {
					//if there is more than 10, get the remaining refunds and process.
					lastRefund := charge.Refunds.Values[len(charge.Refunds.Values)-1]
					listParams := &stripe.RefundListParams{
						ListParams: stripe.ListParams{StripeAccount: evt.Account},
					}
					listParams.Filters.AddFilter("charge", "", charge.ID)
					listParams.Start = lastRefund.ID
					list := api.Refunds.List(listParams)
					for list.Next() {
						processRefund(t, evt, list.Refund(), user, transaction, api, !found)
					}
				}
			}
		} else if found {
			t.Info("transaction: " + transaction.UUID + " already found, skipping...")
		}

		//Process fees on the charge.
		if user.Stripe.ProcessFees {
			t.Info("Generating transaction from fee: " + balanceTransaction.ID)
			feeTrans, found, err := t.TransactionService.FindTransaction(model.GatewayStripe, balanceTransaction.ID)
			if found {
				t.Info("transaction: " + feeTrans.UUID + " already exists, skipping...")
				t.Info("transaction: " + feeTrans.UUID + " already exists, skipping...")
				t.Info("ensuring this transaction has a parent set.")
				if feeTrans.ParentTransaction.UUID == "" {
					t.Info("parent not set for transaction: " + feeTrans.UUID + ", attempting to set...")
					if transaction.UUID != "" {
						feeTrans.ParentTransaction = struct {
							UUID string
							Type string
						}{UUID: transaction.UUID, Type: model.TypeCharge}
						t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(feeTrans)
						t.Info("transaction: " + feeTrans.UUID + ", is now the child of transaction: " + transaction.UUID)
					}
				}
			} else {
				details := balanceTransaction.FeeDetails
				descriptions := make([]string, len(details))
				for _, detail := range details {
					descriptions = append(descriptions, detail.Desc)
				}
				description := strings.Trim(strings.Join(descriptions, ", "), " ,")
				fee = &model.Transaction{
					GatewayID: balanceTransaction.ID,
					GatewayEventID: evt.ID,
					TransactionCreated: time.Unix(balanceTransaction.Created, 0),
					Captured: true,
					Amount: balanceTransaction.Fee,
					Currency: string(balanceTransaction.Currency),
					Status: string(balanceTransaction.Status),
					Description: description,
					GatewayAccountID: evt.Account,
					UserID: user.UUID,
					Gateway: model.GatewayStripe,
					Type: model.TypeFee,
					UUID: uuid.New().String(),
					Version: model.TransactionSchemaVersion,
				}
				if transaction.UUID != "" {
					fee.ParentTransaction = struct {
						UUID string
						Type string
					}{UUID: transaction.UUID, Type: model.TypeCharge}
				}

				if charge.Customer != nil {
					fee.Customer = model.Customer{
						Email: charge.Customer.Email,
						GatewayID: charge.Customer.ID,
					}
				}
				err = t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(fee)
				if err != nil {
					t.Error(err.Error())
				} else {
					t.Info("Transaction " + fee.UUID + " created")
				}
			}
		} else {
			t.Info("Fee processing is disabled for user: " + user.UUID)
		}

		//push this event to the transactions queue.
		t.Notify(event.Webhook{
			Type: event.TransactionCreated,
			Gateway: t.Gateway,
			Event: event.Transaction{
				UUID: user.UUID,
				EventID: evt.ID,
			},
		})

		return nil
	}

	//Handle the post request.
	transactionProcessor.Listen()
}

//processCharge processes and stores the new charge.
func processCharge(
	//t Gives this function access to the transaction processor.
	t *TransactionProcessor,
	//user the user associated with this event.
	user *model.User,
	//charge the stripe charge we are generating a transaction from.
	charge *stripe.Charge,
	//evt The initial stripe event.
	evt *stripe.Event,
	//the balance transaction for the charge.
	txn *stripe.Transaction,
) *model.Transaction {
	trans := &model.Transaction{}
	t.Info("Attempting to generate new transactions")
	//make a transaction for the charge and stripes fee.
	if user.Stripe.ProcessCharges {
		t.Info("Generating transaction from charge: " + charge.ID)
		trans = &model.Transaction{
			GatewayID: charge.ID,
			GatewayEventID: evt.ID,
			TransactionCreated: time.Unix(charge.Created, 0),
			Captured: charge.Captured,
			Refunded: charge.Refunded,
			RefundedAmount: int64(charge.AmountRefunded),
			Amount: int64(charge.Amount),
			Currency: string(charge.Currency),
			Source: struct {
				Type string
				ID   string
			}{Type: string(charge.Source.Type)},
			Status: charge.Status,
			Description: charge.Desc,
			GatewayAccountID: evt.Account,
			UserID: user.UUID,
			Gateway: model.GatewayStripe,
			Type: model.TypeCharge,
			UUID: uuid.New().String(),
			Version: model.TransactionSchemaVersion,
			ConvertedAmount: txn.Amount,
			ConvertedToCurrency: string(txn.Currency),
			NetAmount: txn.Net,
			TotalFees: txn.Fee,
		}

		if charge.Customer != nil {
			trans.Customer = model.Customer{
				Email: charge.Customer.Email,
				GatewayID: charge.Customer.ID,
			}
		}

		switch charge.Source.Type {
		case stripe.PaymentSourceBankAccount:
			trans.Source.ID = charge.Source.BankAccount.ID
			break
		case stripe.PaymentSourceBitcoinReceiver:
			trans.Source.ID = charge.Source.BitcoinReceiver.ID
			break
		case stripe.PaymentSourceCard:
			trans.Source.ID = charge.Source.Card.ID
			break
		}
		err := t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(trans)
		if err != nil {
			t.Error(err.Error())
		} else {
			t.Info("Transaction " + trans.UUID + " created")
		}
	} else {
		t.Info("Charge processing is disabled for user: " + user.UUID)
	}

	return trans
}

//processRefund function which encapsulates the process of processing a charge.refunded event from stripe.
func processRefund(
	//t Gives this function access to the transaction processor.
	t *TransactionProcessor,
	//evt the original stripe event.
	evt *stripe.Event,
	//refund the current refund we are processing.
	refund *stripe.Refund,
	//user the user associated with this event.
	user *model.User,
	//transaction the parent charge transaction for these refunds.
	transaction *model.Transaction,
	//api gives this function access to the Stripe API.
	api *client.API,
	//isNew Whether the transaction is new or not.
	isNew bool,
) error {
	rfdTrans := &model.Transaction{}
	fee := &model.Transaction{}
	var found bool
	var err error
	balanceTransaction, err := api.Balance.GetTx(
		refund.Tx.ID,
		&stripe.TxParams{Params: stripe.Params{StripeAccount: evt.Account}},
	)
	if user.Stripe.ProcessCharges {
		t.Info("Checking to see if the refund with id: " + refund.ID + " has not been processed before.")
		rfdTrans, found, err = t.TransactionService.FindTransaction(model.GatewayStripe, refund.ID)
		if err != nil {
			t.Error(err.Error())
		}
		if found {
			t.Info("transaction " + rfdTrans.UUID + " already found, skipping...")
		} else {
			t.Info("creating new transactions...")
			rfdTrans = &model.Transaction{
				GatewayID: refund.ID,
				GatewayEventID: evt.ID,
				TransactionCreated: time.Unix(refund.Created, 0),
				Captured: true,
				Amount: int64(refund.Amount),
				Currency: string(refund.Currency),
				Status: string(refund.Status),
				GatewayAccountID: evt.Account,
				UserID: user.UUID,
				Gateway: model.GatewayStripe,
				Type: model.TypeRefund,
				UUID: uuid.New().String(),
				Version: model.TransactionSchemaVersion,
				ConvertedAmount: balanceTransaction.Amount,
				ConvertedToCurrency: string(balanceTransaction.Currency),
				TotalFees: balanceTransaction.Fee,
				NetAmount: balanceTransaction.Net,
				Customer: transaction.Customer,
			}

			//if we have an original charge.
			if transaction.UUID != "" && !isNew {
				t.Info("Updating charge: " + transaction.UUID + " with details of this refund.")
				rfdTrans.ParentTransaction = struct {
					UUID string
					Type string
				}{UUID: transaction.UUID, Type: model.TypeCharge}
				//update the original transaction with details of this refund.
				transaction.Refunded = true
				transaction.RefundedAmount += rfdTrans.Amount
				t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(transaction)
			}

			description := "Refund on charge: " + transaction.GatewayID
			if transaction.Refunded && transaction.RefundedAmount < transaction.Amount {
				a := []rune(description)
				a[0] = unicode.ToLower(a[0])
				description = "Partial " + string(a)
			}

			rfdTrans.Description = description

			t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(rfdTrans)
			t.Info("transaction: " + rfdTrans.UUID + " created")
		}
	} else {
		t.Info("Processing charges is disabled for user: " + user.UUID + ", skipping...")
	}

	if user.Stripe.ProcessFees {
		if err != nil {
			return err
		}
		t.Info("Generating transaction from fee: " + balanceTransaction.ID)
		feeTrans, found, _ := t.TransactionService.FindTransaction(model.GatewayStripe, balanceTransaction.ID)
		if found {
			t.Info("transaction: " + feeTrans.UUID + " already exists, skipping...")
			t.Info("ensuring this transaction has a parent set.")
			if feeTrans.ParentTransaction.UUID == "" {
				t.Info("parent not set for transaction: " + feeTrans.UUID + ", attempting to set...")
				if rfdTrans.UUID != "" {
					feeTrans.ParentTransaction = struct {
						UUID string
						Type string
					}{UUID: rfdTrans.UUID, Type: model.TypeRefund}
					t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(feeTrans)
					t.Info("transaction: " + feeTrans.UUID + ", is now the child of transaction: " + rfdTrans.UUID)
				}
			}
		} else {
			details := balanceTransaction.FeeDetails
			descriptions := make([]string, len(details))
			for _, detail := range details {
				descriptions = append(descriptions, detail.Desc)
			}
			description := strings.Trim(strings.Join(descriptions, ", "), " ,")
			fee = &model.Transaction{
				GatewayID: balanceTransaction.ID,
				GatewayEventID: evt.ID,
				TransactionCreated: time.Unix(balanceTransaction.Created, 0),
				Captured: true,
				Amount: balanceTransaction.Fee,
				Currency: string(balanceTransaction.Currency),
				Status: string(balanceTransaction.Status),
				Description: description,
				GatewayAccountID: evt.Account,
				UserID: user.UUID,
				Gateway: model.GatewayStripe,
				Type: model.TypeFee,
				UUID: uuid.New().String(),
				Version: model.TransactionSchemaVersion,
				Customer: transaction.Customer,
			}
			if rfdTrans.UUID != "" {
				fee.ParentTransaction = struct {
					UUID string
					Type string
				}{UUID: rfdTrans.UUID, Type: model.TypeRefund}
			}
			t.AuthService.GetConnection().Collection(model.TransactionCollection).Save(fee)
			t.Info("Transaction " + fee.UUID + " created")
		}
	} else {
		t.Info("Fee processing is disabled for user: " + user.UUID)
	}
	return nil
}
